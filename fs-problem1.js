const fs= require('fs');

// const dir="../async_sync_JS/example";

const createRandomFiles=function fileCreated(dir,error){
fs.mkdir(dir,(err)=>{
    if(err){
        error(console.error(`Error creating directory: ${err}`));
    }
    else{
        console.log(`Directory '${dir}' created successfully.`);
    

        const files = [
            { name: 'file1.json', content: {"name" : "file one", "content": "This is the content of file1."}},
            { name: 'file2.json', content: {"name" : "file two", "content": "This is the content of file2."}},
            { name: 'file3.json', content: {"name" : "file three", "content": "This is the content of file3."}},
        ];
        let count=0;
        files.forEach((file) => {
            const filePath = `${dir}/${file.name}`;

            fs.writeFile(filePath, JSON.stringify(file.content), (err) => {
              if (err) {
                console.error(`Error creating file '${file.name}': ${err}`);
              } else {
                console.log(`File '${file.name}' created successfully.`);
                count++;
                if(count==3) error(null)
                fs.unlink(filePath,(err) =>{
                  if(err){
                      console.error(`Error deleting file '${file.name}': ${err}`);
                  }
                  else{
                      console.log(`File '${file.name}' deleted successfully.`);
                  }
                })
              }
            });  

            
        });

        

    }

})

}

module.exports={
  createRandomFiles,
}