const fs = require('fs');

function problem2(path){
    console.log(__dirname);

    fs.readFile(path, 'utf8', (err, data) => {
        if (err) {
            console.error('Error reading lipsum.txt:', err);
            return;
        }

        const upperCaseContent = data.toUpperCase();
        fs.writeFile('uppercase.txt', upperCaseContent, (err) => {
            if (err) {
                console.error('Error writing to uppercase.txt:', err);
                return;
            }

        
            fs.readFile('uppercase.txt', 'utf8', (err, upperCasedData) => {
                if (err) {
                    console.error('Error reading uppercase.txt:', err);
                    return;
                }

                const lowerCaseContent = upperCasedData.toLowerCase();
                const sentences = lowerCaseContent.split('.');
                const sentenceContent = sentences.join('\n');
                fs.writeFile('sentences.txt', sentenceContent, (err) => {
                    if (err) {
                        console.error('Error writing to sentences.txt:', err);
                        return;
                    }

                    
                    fs.readFile('uppercase.txt', 'utf8', (err, upperCaseData) => {
                        if (err) {
                            console.error('Error reading uppercase.txt:', err);
                            return;
                        }

                        const sortedContent = upperCaseData.split('').sort().join('');
                        fs.writeFile('sorted.txt', sortedContent, (err) => {
                            if (err) {
                                console.error('Error writing to sorted.txt:', err);
                                return;
                            }

                            
                            const filenames = ['uppercase.txt', 'sentences.txt', 'sorted.txt'];
                            fs.writeFile('filenames.txt', filenames.join('\n'), (err) => {
                                if (err) {
                                    console.error('Error writing to filenames.txt:', err);
                                    return;
                                }

                                fs.readFile('filenames.txt', 'utf8', (err, filenamesData) => {
                                    if (err) {
                                        console.error('Error reading filenames.txt:', err);
                                        return;
                                    }

                                    const filesToDelete = filenamesData.split('\n');
                                    deleteFiles(filesToDelete);
                                });
                            });
                        });
                    });
                });
            });
        });
    });

};
function deleteFiles(filenames) {
    let count = filenames.length;
    filenames.forEach((filename) => {
        fs.unlink(filename, (err) => {
            if (err) {
                console.error(`Error deleting file ${filename}:`, err);
                return;
            }

            count--;
            if (count === 0) {
                console.log('All files deleted successfully.');
            }
        });
    });
}


// problem2();
module.exports=problem2;