// test/testFsProblem1.js

const fsProblem1 = require('../fs-problem1');

const dirPath = '../example';
const randomNumberOfFiles = 5;

fsProblem1.createRandomFiles(dirPath, (createErr) => {
  if (createErr) {
    console.error(`Error creating random files: ${createErr}`);
  } 
  else {

    console.log('Random files created successfully.');
  }
});
